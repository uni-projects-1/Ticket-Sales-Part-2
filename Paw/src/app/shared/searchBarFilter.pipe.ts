import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchBarFilter',
})
export class SearchBarFilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    const results: any = new Array();
    if (!items) {
      return [];
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLowerCase();

    var isPatrimony = items[0]['patrimonyId'] != undefined;
    var isEvent = items[0]['eventId'] != undefined;
    var isSale = items[0]['salesId'] != undefined;

    items.forEach((it: any) => {
      if (isPatrimony) {
        if (
          it['name'].toLowerCase().includes(searchText) ||
          it['type'].toLowerCase().includes(searchText) ||
          it['location']['city'].toLowerCase().includes(searchText) ||
          it['location']['district'].toLowerCase().includes(searchText)
        ) {
          results.push(it);
        }
      } else if (isEvent) {
        if (
          it['name'].toLowerCase().includes(searchText) ||
          it['date'].toString().toLowerCase().includes(searchText) ||
          it['type'].toLowerCase().includes(searchText) ||
          it['classification'].toLowerCase().includes(searchText)
        ) {
          results.push(it);
        }
      } else if (isSale) {
        if (
          it['salesId'] == searchText ||
          it['date'].toString().toLowerCase() == searchText
        ) {
          results.push(it);
        }
      }
    });

    return results;
  }
}
