import { TestBed } from '@angular/core/testing';

import { PatrimonyRestService } from './patrimony-rest.service';

describe('PatrimonyRestService', () => {
  let service: PatrimonyRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatrimonyRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
