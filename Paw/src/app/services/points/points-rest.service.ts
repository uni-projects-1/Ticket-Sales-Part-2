import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const endpoint = 'http://localhost:5000/store/point';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class PointsRestService {
  constructor(private httpClient: HttpClient) {}

  getDiscountPer100Points(): Observable<any> {
    return this.httpClient.get<any>(endpoint + '/getDiscountPer100Points');
  }
}
