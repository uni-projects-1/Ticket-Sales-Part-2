import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SaleTicket } from '../models/saleTickets';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { Sale } from '../models/sale';

const endpoint = 'http://localhost:5000/store/sale/';

@Injectable({
  providedIn: 'root',
})
export class SaleServiceService {
  public unfinishedSaleTicket = new BehaviorSubject<any>(null);
  public unfinishedSaleTicket$ = this.unfinishedSaleTicket.asObservable();

  public accessedFromEventDetails: boolean = false;
  private hasAppliedDiscount: boolean = false;

  constructor(private authService: AuthService, private http: HttpClient) {}

  addUnfinishedSaleTicketToCheckout(unfinishedSaleTicket: any): void {
    this.unfinishedSaleTicket.next(unfinishedSaleTicket);
  }

  setAppliedDiscount(hasAppliedDiscount: boolean): void {
    this.hasAppliedDiscount = hasAppliedDiscount;
  }

  getAppliedDiscount(): boolean {
    return this.hasAppliedDiscount;
  }

  registerSale(saleTicket: SaleTicket): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.authService.getToken(),
      }),
    };

    return this.http.post<any>(
      endpoint + 'registerSale',
      JSON.stringify(saleTicket),
      httpOptions
    );
  }

  getSalesByClient(): Observable<Sale[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.authService.getToken(),
      }),
    };

    return this.http.get<Sale[]>(endpoint + 'getSalesByClient', httpOptions);
  }
}
