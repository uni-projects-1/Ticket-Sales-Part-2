import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Patrimony } from '../models/patrimony';


const endpoint = 'http://localhost:5000/store/patrimony/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};


@Injectable({
  providedIn: 'root'
})
export class PatrimonyRestService {
  constructor( private http: HttpClient) { }

  getPatrimony(): Observable<Patrimony[]> {
    return this.http.get<Patrimony[]>(endpoint + 'getAllPatrimonies');
  }

}

