export class LoyaltyInfo {
  constructor(
    public creationMemberDate?: string,
    public ticketsPurchased?: number,
    public totalMoneyPurchased?: number,
    public atualPoints?: number
  ) {}
}
