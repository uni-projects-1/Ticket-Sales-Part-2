export class SaleTicket {
  constructor(
    public vipTickets: number,
    public familyTickets: number,
    public normalTickets: number,
    public childTickets: number,
    public totalTickets: number,
    public eventName: string,
    public clientNIF: number,
    public hasAppliedDiscount: boolean
  ) {}
}
