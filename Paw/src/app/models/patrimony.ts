interface location{
  latitude: number;
  longitude: number;
  city: string;
  district: string;
}

interface eventsRelated{
  ongoingEvents:number [];
  doneEvents:number [] ;
}

interface image {
  publicPath: string;
  typeOfFile: string;
}

export interface Patrimony {
    patrimonyId: number;
    name: string;
    location: location;
    type: string;
    eventsRelated:eventsRelated;
    updated_at: Date;
    image: image;
  }
  