interface eventSeat {
  row: number;
  seat: number;
}

interface image {
  publicPath: string;
  typeOfFile: string;
}

export class Ticket {
  constructor(
    public ticketId: number,
    public name: string,
    public date: Date,
    public eventSeat: eventSeat,
    public type: string,
    public price: number,
    public image: image
  ) {}
}
