interface Client {
  clientId: number;
  name: string;
  nif: number;
  cellPhone: number;
  email: string;
  pointsBeforeSale: number;
}

interface EventSeat {
  row: number;
  seat: number;
}

interface ImageTicket {
  staticUrl: string;
  type: string;
}

interface Ticket {
  _id: string;
  ticketId: number;
  name: string;
  local: string;
  date: Date;
  hour: Date;
  eventSeat: EventSeat;
  type: string;
  quantity: number;
  price: number;
  imageTicket: ImageTicket;
}

export class Sale {
  constructor(
    public salesId: number,
    public client: Client,
    public tickets: Ticket[],
    public totalTicketsBought: number,
    public totalValue: number,
    public pointsToDiscount: number,
    public totalValueWithDiscount: number,
    public discountValuePer100Points: number,
    public date: Date,
  ) {}
}
