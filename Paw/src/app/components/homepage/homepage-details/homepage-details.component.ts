import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';

import { Event } from '../../../models/event';

import { HomepageComponent } from '../homepage.component';
import { EventRestService } from '../../../services/event-rest.service';
import { SaleServiceService } from 'src/app/services/sale.service.service';
import { TicketRestService } from 'src/app/services/tickets/ticket-rest.service';
import { Ticket } from 'src/app/models/ticket';

@Component({
  selector: 'app-homepage-details',
  templateUrl: './homepage-details.component.html',
  styleUrls: ['./homepage-details.component.css'],
})
export class HomepageDetailsComponent {
  @Input() event!: Event;

  validationErrors: number[] = [];

  vipTickets!: number;
  familyTickets!: number;
  normalTickets!: number;
  childTickets!: number;

  vipTicketPrice!: number;
  familyTicketPrice!: number;
  normalTicketPrice!: number;
  childTicketPrice!: number;

  doesEventHaveTickets: boolean = true;

  errorChildTickets: boolean = false;
  errorFamilyTickets: boolean = false;
  errorNormalTickets: boolean = false;
  errorVipTickets: boolean = false;
  errorNoTickets: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private patrimoniesComponent: HomepageComponent,
    private eventRestService: EventRestService,
    private ticketsRestService: TicketRestService,
    private saleService: SaleServiceService
  ) {}

  ngOnInit(): void {
    this.loadEvent();
    this.loadTicketsForEvent();
  }

  loadEvent(): void {
    if (this.event == undefined) {
      let nameInUrl = this.route.snapshot.paramMap.get('name');
      if (sessionStorage.getItem('events')) {
        const events = JSON.parse(sessionStorage.getItem('events') || '[]');
        this.event = events.find((event: Event) => {
          return event.name === nameInUrl;
        });
      } else {
        this.eventRestService
          .getEventByName(nameInUrl)
          .subscribe((event: Event) => {
            this.event = event;
          });
      }
    }
  }

  loadTicketsForEvent(): void {
    this.ticketsRestService
      .getTicketsForEvent(this.event.name)
      .subscribe((tickets: Ticket[]) => {
        if (tickets.length == 0) {
          this.doesEventHaveTickets = false;
          // Make the div hidden
          (document.getElementById('buyDiv') as HTMLDivElement)?.setAttribute(
            'style',
            'display: none'
          );
          (
            document.getElementById('buyButton') as HTMLButtonElement
          )?.setAttribute('style', 'display: none');
        } else {
          this.vipTicketPrice = tickets[0].price;
          this.normalTicketPrice = tickets[1].price;
          this.familyTicketPrice = tickets[2].price;
          this.childTicketPrice = tickets[3].price;
        }
      });
  }

  printErrors() {
    this.validationErrors.forEach((error) => {
      debugger;
      switch (error) {
        case 1:
          this.errorVipTickets = true;
          window.scrollTo(0, 0);
          break;
        case 2:
          this.errorFamilyTickets = true;
          window.scrollTo(0, 0);
          break;
        case 3:
          this.errorNormalTickets = true;
          window.scrollTo(0, 0);
          break;
        case 4:
          this.errorChildTickets = true;
          window.scrollTo(0, 0);
          break;
        default:
          this.errorNoTickets = true;
          window.scrollTo(0, 0);
          break;
      }
    });

    this.validationErrors = [];
  }

  confirmCheckout(): void {
    const vipTicketsInput = document.getElementById(
      'vipTickets'
    ) as HTMLInputElement;
    const familyTicketsInput = document.getElementById(
      'familyTickets'
    ) as HTMLInputElement;
    const normalTicketsInput = document.getElementById(
      'normalTickets'
    ) as HTMLInputElement;
    const childTicketsInput = document.getElementById(
      'childTickets'
    ) as HTMLInputElement;

    // Calculate the total tickets to be sent
    this.vipTickets =
      vipTicketsInput && vipTicketsInput.value.trim() !== ''
        ? parseInt(vipTicketsInput.value)
        : 0;
    this.familyTickets =
      familyTicketsInput && familyTicketsInput.value.trim() !== ''
        ? parseInt(familyTicketsInput.value)
        : 0;
    this.normalTickets =
      normalTicketsInput && normalTicketsInput.value.trim() !== ''
        ? parseInt(normalTicketsInput.value)
        : 0;
    this.childTickets =
      childTicketsInput && childTicketsInput.value.trim() !== ''
        ? parseInt(childTicketsInput.value)
        : 0;

    if (this.vipTickets < 0 || this.vipTickets > 10) {
      this.validationErrors.push(1);
    }

    if (this.familyTickets < 0 || this.familyTickets > 10) {
      this.validationErrors.push(2);
    }

    if (this.normalTickets < 0 || this.normalTickets > 10) {
      this.validationErrors.push(3);
    }

    if (this.childTickets < 0 || this.childTickets > 10) {
      this.validationErrors.push(4);
    }

    const totalTickets =
      this.vipTickets +
      this.familyTickets +
      this.normalTickets +
      this.childTickets;

    if (totalTickets == 0) {
      this.validationErrors.push(5);
    }

    const totalPrice =
      this.vipTickets * this.vipTicketPrice +
      this.familyTickets * this.familyTicketPrice +
      this.normalTickets * this.normalTicketPrice +
      this.childTickets * this.childTicketPrice;

    // Since we have no client NIF or discount, we send the unfinished sale ticket to the checkout component
    const unfinishedSaleTicket = {
      eventName: this.event.name,
      vipTickets: this.vipTickets,
      familyTickets: this.familyTickets,
      normalTickets: this.normalTickets,
      childTickets: this.childTickets,
      totalTickets: totalTickets,
      totalPrice: totalPrice,
    };

    if (this.validationErrors.length === 0) {
      // Use the sale service to send the unfinished sale ticket to the checkout component
      this.saleService.addUnfinishedSaleTicketToCheckout(unfinishedSaleTicket);
      this.saleService.accessedFromEventDetails = true;
      // Navigate to the checkout component
      this.router.navigate(['checkout']);
    } else {
      this.printErrors();
    }
  }

  closeError(errorNumber: number) {
    switch (errorNumber) {
      case 1:
        this.errorVipTickets = false;
        break;
      case 2:
        this.errorFamilyTickets = false;
        break;
      case 3:
        this.errorNormalTickets = false;
        break;
      case 4:
        this.errorChildTickets = false;
        break;
      default:
        this.errorNoTickets = false;
        break;
    }
  }
}
