import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimoniesComponent } from './patrimonies.component';

describe('PatrimoniesComponent', () => {
  let component: PatrimoniesComponent;
  let fixture: ComponentFixture<PatrimoniesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PatrimoniesComponent]
    });
    fixture = TestBed.createComponent(PatrimoniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
