import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimoniesDetailComponent } from './patrimonies-detail.component';

describe('PatrimoniesDetailComponent', () => {
  let component: PatrimoniesDetailComponent;
  let fixture: ComponentFixture<PatrimoniesDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PatrimoniesDetailComponent]
    });
    fixture = TestBed.createComponent(PatrimoniesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
