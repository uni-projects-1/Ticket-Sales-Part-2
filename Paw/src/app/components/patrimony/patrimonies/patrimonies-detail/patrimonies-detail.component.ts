import { Component, Input, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { PatrimonyRestService } from '../../../../services/patrimony-rest.service';
import { Patrimony } from '../../../../models/patrimony';
import { PatrimoniesComponent } from '../patrimonies.component';
import { Event } from 'src/app/models/event';

@Component({
  selector: 'app-patrimonies-detail',
  templateUrl: './patrimonies-detail.component.html',
  styleUrls: ['./patrimonies-detail.component.css'],
})
export class PatrimoniesDetailComponent implements OnInit {
  @Input() patrimony!: Patrimony;
  doneEvents: Event[] = [];
  ongoingEvents: Event[] = [];

  constructor(
    private router: Router,
    private patrimoniesComponent: PatrimoniesComponent
  ) {}

  ngOnInit(): void {
    this.getRelatedEvent();
  }

  getRelatedEvent(): void {
    if (sessionStorage.getItem('events')) {
      const events = JSON.parse(sessionStorage.getItem('events') || '[]');

      const doneEvents = this.patrimony.eventsRelated.doneEvents;
      const ongoingEvents = this.patrimony.eventsRelated.ongoingEvents;

      this.doneEvents = events.filter((event: Event) => {
        return doneEvents.includes(event.eventId);
      });
      this.ongoingEvents = events.filter((event: Event) => {
        return ongoingEvents.includes(event.eventId);
      });
    }
  }

  closePatrimonyDetails(): void {
    this.patrimoniesComponent.closePatrimonyDetails();
  }
}
