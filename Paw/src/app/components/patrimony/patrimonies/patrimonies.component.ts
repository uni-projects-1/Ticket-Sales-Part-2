import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PatrimonyRestService } from '../../../services/patrimony-rest.service';
import { Patrimony } from '../../../models/patrimony';
import { SearchBarService } from 'src/app/services/searchbar/search-bar.service';

@Component({
  selector: 'app-patrimonies',
  templateUrl: './patrimonies.component.html',
  styleUrls: ['./patrimonies.component.css'],
})
export class PatrimoniesComponent implements OnInit {
  patrimonies: Patrimony[] = [];
  p: number = 0;

  listing!: boolean;
  clickedPatrimony!: Patrimony;

  searchText: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventRestService: PatrimonyRestService,
    private searchBar: SearchBarService
  ) {
    this.listing = true;
  }

  ngOnInit(): void {
    this.getPatrimony();
    this.searchBar.searchText.subscribe((searchText) => {
      this.searchText = searchText;
    });
  }

  getPatrimony(): void {
    this.eventRestService
      .getPatrimony()
      .subscribe((patrimonies: Patrimony[]) => {
        this.patrimonies = patrimonies;
      });
  }

  showDetails(patrimony: Patrimony): void {
    this.clickedPatrimony = patrimony;
    this.listing = false;
  }

  closePatrimonyDetails(): void {
    this.listing = true;
  }
}
