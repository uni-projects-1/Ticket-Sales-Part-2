import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SearchBarService } from 'src/app/services/searchbar/search-bar.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  searchTerm!: string;
  isMenuOpen: boolean = false;

  constructor(private searchBar: SearchBarService, private auth: AuthService) {}

  search(event: any) {
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.searchBar.searchText.next(this.searchTerm);
  }

  logout() {
    this.auth.logout();
  }

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }
}
