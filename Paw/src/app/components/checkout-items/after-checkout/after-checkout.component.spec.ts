import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterCheckoutComponent } from './after-checkout.component';

describe('AfterCheckoutComponent', () => {
  let component: AfterCheckoutComponent;
  let fixture: ComponentFixture<AfterCheckoutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AfterCheckoutComponent]
    });
    fixture = TestBed.createComponent(AfterCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
