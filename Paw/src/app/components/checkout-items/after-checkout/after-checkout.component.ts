import { Component } from '@angular/core';
import { EventRestService } from 'src/app/services/event-rest.service';
import { SaleServiceService } from 'src/app/services/sale.service.service';

@Component({
  selector: 'app-after-checkout',
  templateUrl: './after-checkout.component.html',
  styleUrls: ['./after-checkout.component.css'],
})
export class AfterCheckoutComponent {
  constructor(
    private eventService: EventRestService,
    private saleService: SaleServiceService
  ) {}

  relatedEvents: string[] = [];

  eventName!: string;

  ngOnInit(): void {
    this.getEventNameFromCheckout();
  }

  getEventNameFromCheckout() {
    this.saleService.unfinishedSaleTicket.subscribe(
      (data: any) => (this.eventName = data)
    );
    this.getRelatedEvents();
  }

  getRelatedEvents() {
    this.eventService
      .getRelatedEvents(this.eventName)
      .subscribe((data: string[]) => {
        this.relatedEvents = data;
      });
  }
}
