
# Paw Milestone 2

## Installation

-   Run `npm install` in the console of the Backend.
-   Run `npm install` in the console of the Frontend.

## Usage

-   Run `npm start` in the console of the Backend.

Access the FrontOffice by visiting [http://localhost:4200/](http://localhost:4200/) or clicking the FrontOffice button inside [http://localhost:5000/](http://localhost:5000/).

## Menu Structure

The application's menu structure is as follows:

1.  **VENUES**
    
    -   Click on a card to view patrimony information.
        -   Click "Go Back" to list all venues.
        -   Click "Ongoing/Done Events" to view related event information.
2.  **EVENTS**
    
    -   Click on a card to view event information.
        -   Click "Go Back" to list all events.
        -   Input the ticket quantity and click "Buy" to purchase tickets (leads to CHECKOUT).
3.  **CHECKOUT**
    
    -   Login/Register to unlock the Checkout.
    -   Confirm the purchase.
    -   Confirm the information.
        -   Click on interesting/related events (optional).
4.  **PROFILE**
    
    -   View Profile Information.
        -   Edit Information.
        -   View Sales (derived from Checkout).
    -   Logout.

## Footer

The Footer section contains:

-   **ABOUT US**
    -   Click on the text "About Us" to view the information.